/* eslint-disable @typescript-eslint/adjacent-overload-signatures */
/* enums */

enum Programme {
    ndef   = 0,
    short  = 10,   // ~3 minutes (exactly 190 seconds) of breathing exercise
    medium = 16,   // ~5 minutes (exactly 304 seconds) of breathing exercise
    long   = 29    // ~9 minutes (exactly 551 seconds) of breathing exercise
}

enum State {
    ndef   = 0,
    in     = 4,
    hold   = 7,
    out    = 8
}

/* events */
//let stopTimer = new CustomEvent("stopTimer",{detail: 3});

/* export */ 
class BreathingModel {

    /* fields */

    private readonly _in:    number = 4;   // time of inhaling
    private readonly _hold:  number = 7;   // time of holding breath
    private readonly _out:   number = 8;   // time of exhaling

    private _prog:  Programme;
    private _state: State;

    private _time:  number;
    private _cnt:   number;

    /* getters */

    public get in()     { return this._in; }
    public get hold()   { return this._hold; }
    public get out()    { return this._out; }
    public get prog()   { return this._prog; }
    public get state()  { return this._state; }
    public get time()   { return this._time; }
    public get cnt()    { return this._cnt; }
    
    /* setters */

    public set prog(p: Programme) {
        this._prog = p;
        this._cnt = +p - 1;
        p === 0 ? this._state = State.ndef : this._state = State.in;
    }

    public set state(s: State) {
        this._state = s;
        this._time = +s;
    }

    public set time(t: number) {
        if (t < 0) {
            throw new Error('Time is not valid!');
        }
        this._time = t;
    }

    public set cnt(t: number) {
        if (t < -1) {
            throw new Error('Count of laps is not valid!');
        }
        this._cnt = t;
    }

    /* constructor */

    constructor() {
        this._prog  = Programme.ndef;   // programme is not defined
        this._state = State.ndef;       // since no programme has started yet, state is not defined
        this._time  = +this._in;        // remaining time in seconds
        this._cnt   = +this._prog-1;    // remaining laps
    }

    /* private methods */

    private checkTime(): boolean {

        if (this._time <= 0) {

            let s = 0;
            if (this._state === State.in)   s=1;
            if (this._state === State.hold) s=2;
            if (this._state === State.out)  s=3;

            switch (s)
            {
                case 1:
                    this._state = State.hold;
                    break;
                    
                case 2:
                    this._state = State.out;
                    break;

                case 3:
                    this._cnt--;
                    if (this._cnt === 0)
                    {
                        this._prog = Programme.ndef; 
                        this._state = State.ndef;
                    }
                    else this._state = State.in;
                    break;

                default:
                    break;
            }

            this._time = +this._state;
            return false;
        }

        return true;
    }

    /* public methods */
    
    public Tick(): number {
        if (this.prog !== Programme.ndef && this.checkTime()) this._time--;
        return +this._state;
    }

    public StringState(): string {
        switch (this._state) {
            case State.in:
                return "belégzés";
            case State.hold:
                return "tart";
            case State.out:
                return "kilégzés";
            default:
                return "";
        }
    }

}
class BreathingView {

    /* fields */

    private _model:         BreathingModel; 

    private _shortButton:   HTMLButtonElement;
    private _mediumButton:  HTMLButtonElement;
    private _longButton:    HTMLButtonElement;

    private _circle:        HTMLElement;
    private _timer:         number;

    /* constructor */

    constructor() {
        this._shortButton  = document.getElementById("short")!  as HTMLButtonElement;
        this._mediumButton = document.getElementById("medium")! as HTMLButtonElement;
        this._longButton   = document.getElementById("long")!   as HTMLButtonElement;

        this._circle       = document.getElementById("circle")!;
        this._timer        = 0;

        this._shortButton.addEventListener("click", (e:Event)  => this.ShortButtonPressed());
        this._mediumButton.addEventListener("click", (e:Event) => this.MediumButtonPressed());
        this._longButton.addEventListener("click", (e:Event)   => this.LongButtonPressed());
        
        this._model = new BreathingModel();

        document.querySelector('#temp')!.innerHTML = "";
    }

    /* private methods */

    private hide() {
        this._circle.style.visibility    = "visible";
        this._shortButton.style.display  = 'none';
        this._mediumButton.style.display = 'none';
        this._longButton.style.display   = 'none';
    }

    private show() {
        this._circle.style.visibility    = "hidden";
        this._shortButton.style.display  = 'inline-block';
        this._mediumButton.style.display = 'inline-block';
        this._longButton.style.display   = 'inline-block';
    }

    private Tick(): void {
        let n = breathingView._model.Tick();
        n = +breathingView._model.state;
        switch (n) {
            case 0:
                this.show();
                break;
            case 4: //in
                this._circle.style.background = "#DBA9AC";
                break;
            case 7: //hold
                this._circle.style.background = "#CC6666";
                break;
            case 8: //out
                this._circle.style.background = "#834751";
                break;
            default:
                break;
        }
        
        document.querySelector('#temp')!.innerHTML = 
        String(new Date(this._model.time * 1000).toISOString().substr(14, 5)) + 
        " <br>Státusz: " + this._model.StringState() + 
        " <br>Hátramaradt körök száma: " + String(this._model.cnt.toString());
    }

    /* private slots */

    private ShortButtonPressed() {
        this._model.prog = Programme.short;
        this.hide();
        this._timer = window.setInterval(() => breathingView.Tick(), 1000);
    }

    private MediumButtonPressed() {
        this._model.prog = Programme.medium;
        this.hide();
        this._timer = window.setInterval(() => breathingView.Tick(), 1000);
    }

    private LongButtonPressed() {
        this._model.prog = Programme.long;
        this.hide();
        this._timer = window.setInterval(() => breathingView.Tick(), 1000);
    }

}
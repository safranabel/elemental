
enum PenType {
  standard,
  filler,
}

const lineRefreshed: CustomEvent = new CustomEvent("lineRefreshed");
const rectRefreshed: CustomEvent = new CustomEvent('rectRefreshed');


class ColouringModel {
  /* fields */
  private actualPenType: PenType;
  private penRadius: number;
  private colour: string;
  private tableHeight:number;
  private tableWidth:number;
  private lastCoordinates: [number, number];
  private actualCoordinates: [number, number];
  private rectRigthBottomCoords: [number, number];
  private rectLeftTopCoords: [number, number];
  private inDrawing: boolean;
  private firstDraw: boolean;

  /* getters and setters */
  public getPenType(): PenType {
    return this.actualPenType;
  }
  public setPenType(pentype: PenType): void {
    this.actualPenType = pentype;
  }
  public getPenRadius(): number {
    return this.penRadius;
  }
  public getColour(): string {
    return this.colour;
  }
  public getDrawableLine():[[number, number],[number, number]]{
    return [this.lastCoordinates,this.actualCoordinates];
  }
  public getDrawableRect():[[number, number],[number, number]]{
    return [this.rectLeftTopCoords,this.rectRigthBottomCoords];
  }
  public getHeigth(): number{
    return this.tableHeight;
  }
  public getWidth(): number{
    return this.tableWidth;
  }

  /* public methods */
  public startDrawing(): void {
    this.inDrawing = true;
  }

  public stopDrawing(): void {
    this.inDrawing = false;
    this.firstDraw = true;
  }

  public penMovedTo(coords: [number, number]): void {

    if (!this.inDrawing) {
      return;
    }

    if(0 > coords[0] || coords[0] > this.tableWidth || 0 > coords[1] || coords[1] > this.tableHeight){
      return;
    }

    if (this.actualPenType === PenType.filler) {
      // not yet implemented
      return;
    }

    if(this.firstDraw){
      this.lastCoordinates = coords;
      this.actualCoordinates = coords;
      this.firstDraw = false;
    }
    const fromX: number = coords[0] - this.penRadius;
    const fromY: number = coords[1] - this.penRadius;
    const toX: number = coords[0] + this.penRadius;
    const toY: number = coords[1] + this.penRadius;

    this.lastCoordinates = this.actualCoordinates;
    this.actualCoordinates = coords;
    dispatchEvent(lineRefreshed);
  }
  public increasePenRadius(amount = 1): void{
    if(this.penRadius + amount < 2){
      this.penRadius = 2;
    } else if(this.penRadius + amount > 30){
      this.penRadius = 30;
    } else{
      this.penRadius+=amount;
    }
  }
  public changeBackgroundColour(colour:string):void{
      const actualColour = this.colour;
      this.colour = colour;
      this.rectLeftTopCoords = [0,0];
      this.rectRigthBottomCoords = [this.tableWidth, this.tableHeight];

      dispatchEvent(rectRefreshed);

      this.rectLeftTopCoords = [0,0];
      this.rectRigthBottomCoords = [0,0];

      this.colour = actualColour;
  }
  public changePenColour(colour:string):void{
    const colourRegExp = '^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$';
    if(!colour.match(colourRegExp)){
      return;
    }
    this.colour = colour;
  }

  /* constructors */
  constructor(tableheight = 300, tablewidth = 400) {
    this.tableHeight = tableheight;
    this.tableWidth = tablewidth;
    this.actualPenType = PenType.standard;
    this.penRadius = 3;
    this.inDrawing = false;
    this.lastCoordinates = [0, 0];
    this.actualCoordinates = [0, 0];
    this.rectLeftTopCoords = [0, 0];
    this.rectRigthBottomCoords = [0, 0];
    this.colour = "#FF0000";
    this.firstDraw = true;

  
  }
}

class ColouringView{
    
    /* fields */
    private model:ColouringModel;
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private increaseRadiusButton: HTMLButtonElement;
    private reduceRadiusButton: HTMLButtonElement;
    private bgColouringButtons: NodeList;
    private penColourButtons: NodeList;


    /* private methods */
   

    /* private slots */
    private mouseInDrawingMode(e:MouseEvent): void{
        
        colouringView.model.startDrawing();
        const canvaspos = colouringView.canvas.getBoundingClientRect();
    }
    private mouseOutFromDrawingMode(e:MouseEvent):void{
        colouringView.model.stopDrawing();
    }
    private mouseMoved(e:MouseEvent){
        const canvaspos = colouringView.canvas.getBoundingClientRect();
        colouringView.model.penMovedTo([e.clientX-canvaspos.left, e.clientY-canvaspos.top]);
       
    }
    private refreshLineOnCanvas(e:Event):void{
        const lineData = colouringView.model.getDrawableLine();
        
        colouringView.context.lineWidth = colouringView.model.getPenRadius()*2;
        colouringView.context.beginPath();
        colouringView.context.strokeStyle = colouringView.model.getColour();
        colouringView.context.moveTo(lineData[0][0], lineData[0][1]);
        colouringView.context.lineTo(lineData[1][0], lineData[1][1]);
        colouringView.context.stroke();

        colouringView.context.lineWidth = colouringView.model.getPenRadius();
        colouringView.context.beginPath();
        colouringView.context.fillStyle = colouringView.model.getColour();

        colouringView.context.arc(lineData[1][0], lineData[1][1], colouringView.model.getPenRadius()/2, 0, 2*Math.PI, false);
        colouringView.context.stroke();        
        
    }
    private refreshRectOnCanvas(e:Event):void{
        const rectData = colouringView.model.getDrawableRect();
        
        colouringView.context.fillStyle = colouringView.model.getColour();
        colouringView.context.moveTo(0, 0);
        colouringView.context.fillRect(rectData[0][0], rectData[0][1], rectData[1][0], rectData[1][1]);
        console.log(e);
    }
    private reduceButtonClicked(e:Event):void{
        colouringView.model.increasePenRadius(-1);
    }
    private increaseButtonClicked(e:Event):void{
        colouringView.model.increasePenRadius(1);
    }
    private bgColouringButtonClicked (e:Event):void {
        const colour = (e.target! as HTMLButtonElement).name;
        colouringView.model.changeBackgroundColour(colour);
    }
    private penColourButtonClicked(e:Event):void {
        const colour = (e.target! as HTMLButtonElement).name;
        colouringView.model.changePenColour(colour);
    }

    
    constructor(){
        this.canvas = document.querySelector('#colouringCanvas')! as HTMLCanvasElement
        this.context = this.canvas.getContext('2d')!;  
        this.canvas.addEventListener('mousedown', this.mouseInDrawingMode);
        document.addEventListener('mouseup', this.mouseOutFromDrawingMode);
        this.canvas.addEventListener('mousemove', this.mouseMoved);
        
        this.reduceRadiusButton = document.querySelector('#reduceRadiusButton')! as HTMLButtonElement;
        this.increaseRadiusButton = document.querySelector('#increaseRadiusButton')! as HTMLButtonElement;
        this.reduceRadiusButton.addEventListener('click', this.reduceButtonClicked);
        this.increaseRadiusButton.addEventListener('click', this.increaseButtonClicked);

        this.bgColouringButtons = document.querySelectorAll('#bgColourButtonsDiv button');
        this.bgColouringButtons.forEach(button => {
            button.addEventListener('click', this.bgColouringButtonClicked);
        });

        this.penColourButtons = document.querySelectorAll('#penColourButtonsDiv button');
        this.penColourButtons.forEach(button => {
            button.addEventListener('click', this.penColourButtonClicked);
        });

        this.canvas.height = 300;
        this.canvas.width = 400;
        addEventListener('lineRefreshed', this.refreshLineOnCanvas);
        addEventListener('rectRefreshed', this.refreshRectOnCanvas);
        this.model = new ColouringModel(this.canvas.height, this.canvas.width);
    }
}


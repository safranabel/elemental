enum IntervalType {
    study,
    break
}

enum IntervalStatus {
    on,
    off
}

const stopTimer = new CustomEvent("stopTimer",{detail: 3});

class PomodoroModel {

    private studyTime: number;
    private breakTime: number;
    private remainingTime: number;
    private intervalType: IntervalType;
    private intervalStatus: IntervalStatus;
    private studyMinutes: number;
    private breakMinutes: number;

    constructor(){
        this.studyTime = 1500; // default study time is 25 minutes
        this.breakTime = 300; // default break time is 5 minutes
        this.remainingTime = this.studyTime; // timer starts with studying interval
        this.intervalType = IntervalType.study;
        this.intervalStatus = IntervalStatus.off;
        this.studyMinutes = 25;
        this.breakMinutes = 5;
    }

    /* getters */
    public getStudyTime(){
        return this.studyTime;
    }

    public getBreakTime(){
        return this.breakTime;
    }

    public getRemainingTime(){
        return this.remainingTime;
    }

    public getIntervalType(){
        return this.intervalType;
    }

    public getIntervalStatus(){
        return this.intervalStatus;
    }

    public getStudyMinutes(){
        return this.studyMinutes;
    }

    public getBreakMinutes(){
        return this.breakMinutes;
    }

    /* setters */
    public setStudyTime(time: number){
        this.studyTime = time;
    }
    
    public setBreakTime(time: number){
        this.breakTime = time;
    }

    public setRemainingTime(time: number){
        this.remainingTime = time;
    }

    public setIntervalStatus(status: IntervalStatus){
        this.intervalStatus = status;
    }

    public setStudyMinutes(time: number){
        this.studyMinutes = time;
    }

    public setBreakMinutes(time: number){
        this.breakMinutes = time;
    }

    /* methods */
    private checkTime(): boolean {
        if(this.remainingTime === 0 && this.intervalType === IntervalType.study){
            this.intervalType = IntervalType.break;
            this.remainingTime = this.breakTime;
            this.intervalStatus = IntervalStatus.off;
            dispatchEvent(stopTimer);
            return false;
        } else if(this.remainingTime === 0 && this.intervalType === IntervalType.break){
            this.intervalType = IntervalType.study;
            this.remainingTime = this.studyTime;
            this.intervalStatus = IntervalStatus.off;
            dispatchEvent(stopTimer);
            return false;
        }

        return true;
    }

    public passTime(): void{
        if(this.intervalStatus === IntervalStatus.on && this.checkTime()){
            this.remainingTime--;
        }
    }

    public startTimer(){
        if(this.intervalStatus === IntervalStatus.on){
            return;
        }
        this.intervalStatus = IntervalStatus.on;
    }

    public pauseTimer(){
        this.intervalStatus = IntervalStatus.off;
    }

    public skipInterval(): void{
        if(this.intervalType === IntervalType.study){
            this.intervalType = IntervalType.break;
            this.remainingTime = this.breakTime;
            this.intervalStatus = IntervalStatus.off;
            //dispatchEvent(stopTimer);
        } else if(this.intervalType === IntervalType.break){
            this.intervalType = IntervalType.study;
            this.remainingTime = this.studyTime;
            this.intervalStatus = IntervalStatus.off;
            //dispatchEvent(stopTimer);
        }
    }

    public increaseStudyMinutes(){
        if(this.studyMinutes + 7 >= 60){
            return;
        }
        this.studyMinutes += 7;
    }

    public decreaseStudyMinutes(){
        if(this.studyMinutes - 7 <= 0){
            return;
        }
        this.studyMinutes -= 7;
    }

    public increaseBreakMinutes(){
        if(this.breakMinutes + 7 >= 41){
            return;
        }
        this.breakMinutes += 7;
    }

    public decreaseBreakMinutes(){
        if(this.breakMinutes - 7 <= 0){
            return;
        }
        this.breakMinutes -= 7;
    }
}
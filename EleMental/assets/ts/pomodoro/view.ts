class PomodoroView {
    private _model: PomodoroModel;
    private startButton: HTMLButtonElement;
    private pauseButton: HTMLButtonElement;
    private skipButton: HTMLButtonElement;
    private studyPlusButton: HTMLButtonElement;
    private breakPlusButton: HTMLButtonElement;
    private studyMinusButton: HTMLButtonElement;
    private breakMinusButton: HTMLButtonElement;
    private setIntervals: HTMLButtonElement;
    private timer: number;

    constructor(){
        this.startButton = document.querySelector('#startButton')! as HTMLButtonElement;
        this.pauseButton = document.querySelector('#pauseButton')! as HTMLButtonElement;
        this.skipButton = document.querySelector('#skipButton')! as HTMLButtonElement;
        this.studyPlusButton = document.querySelector('#studyPlusButton')! as HTMLButtonElement;
        this.breakPlusButton = document.querySelector('#breakPlusButton')! as HTMLButtonElement;
        this.studyMinusButton = document.querySelector('#studyMinusButton')! as HTMLButtonElement;
        this.breakMinusButton = document.querySelector('#breakMinusButton')! as HTMLButtonElement;
        this.setIntervals = document.querySelector('#setIntervals')! as HTMLButtonElement;
        this.timer = 0;

        this.startButton.addEventListener('click', this.StartTimer);
        this.pauseButton.addEventListener('click', this.PauseTimer);
        this.skipButton.addEventListener('click', this.SkipInterval);
        this.studyPlusButton.addEventListener('click', this.IncreaseStudyMinutes);
        this.breakPlusButton.addEventListener('click', this.IncreaseBreakMinutes);
        this.studyMinusButton.addEventListener('click', this.DecreaseStudyMinutes);
        this.breakMinusButton.addEventListener('click', this.DecreaseBreakMinutes);
        this.setIntervals.addEventListener('click', this.SetIntervals);

        addEventListener('stopTimer', this.PauseTimer);
        this._model = new PomodoroModel();
        document.querySelector('#time')!.innerHTML = String(new Date(this._model.getRemainingTime() * 1000).toISOString().substr(14, 5));
    }

    private PassTime(): void {
        view._model.passTime();
        console.log(view._model.getRemainingTime());
            
        document.querySelector('#time')!.innerHTML = String(new Date(view._model.getRemainingTime() * 1000).toISOString().substr(14, 5));
    }

    private StartTimer(): void {
        if(view._model.getIntervalStatus() === IntervalStatus.on){
            return;
        }
        view._model.startTimer();
        view.timer = window.setInterval(() => view.PassTime(), 1000);
    }

    private PauseTimer(): void {
        view._model.pauseTimer();
        clearInterval(view.timer);
    }

    private SkipInterval(){
        view._model.skipInterval();
        clearInterval(view.timer);
        document.querySelector('#time')!.innerHTML = String(new Date(view._model.getRemainingTime() * 1000).toISOString().substr(14, 5));
    }

    private SetIntervals(){
        view.PauseTimer();
        view._model.setStudyTime(view._model.getStudyMinutes()*60);
        view._model.setRemainingTime(view._model.getStudyMinutes()*60);
        console.log(view._model.getStudyTime());
        document.querySelector('#time')!.innerHTML = String(new Date(view._model.getStudyTime() * 1000).toISOString().substr(14, 5));
        
        view._model.setBreakTime(view._model.getBreakMinutes()*60);
        
    }

    private IncreaseStudyMinutes(){
        view._model.increaseStudyMinutes();
        document.querySelector('#studyMinutes')!.innerHTML = String(view._model.getStudyMinutes());
    }

    private DecreaseStudyMinutes(){
        view._model.decreaseStudyMinutes();
        document.querySelector('#studyMinutes')!.innerHTML = String(view._model.getStudyMinutes());
    }

    private IncreaseBreakMinutes(){
        view._model.increaseBreakMinutes();
        document.querySelector('#breakMinutes')!.innerHTML = String(view._model.getBreakMinutes());
    }

    private DecreaseBreakMinutes(){
        view._model.decreaseBreakMinutes();
        document.querySelector('#breakMinutes')!.innerHTML = String(view._model.getBreakMinutes());
    }
}
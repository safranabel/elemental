/* 
A test case should look like:

QUnit.test('[{pagename}][{class}] Test {details}',
    assert => {
        assert.true(true);
    }
);
*/

QUnit.test('[breathing page][model] Test default settings',
    assert => {
        const model = new BreathingModel();

        const prog = model.prog;
        const state = model.state;
        const time = model.time;
        const cnt = model.cnt;

        assert.equal(prog, 0, "Default programme should be ndef.");
        assert.equal(state, 0, "Default state should be ndef.");
        assert.equal(time, 4, "Default time should be 4 (first step will be breathing in).");
        assert.equal(cnt, -1, "Default remaining laps should be -1.");
    }
);

QUnit.test('[breathing page][model] Test setters',
    assert => {
        const model = new BreathingModel();

        // Setting programme to ndef.
        model.prog = 0;
        let prog = model.prog;
        let cnt = model.cnt;
        let state = model.state;
        assert.equal(prog, 0, "Programme should be 0 (ndef) now.");
        assert.equal(cnt, -1, "Laps should be -1 now.");
        assert.equal(state, 0, "State should be 0 (ndef) now.");

        // Setting programme to short.
        model.prog = 10;
        prog = model.prog;
        cnt = model.cnt;
        state = model.state;
        assert.equal(prog, 10, "Programme should be 10 (short) now.");
        assert.equal(cnt, 9, "Laps should be 9 now.");
        assert.equal(state, 4, "State should be 4 (in) now.");

        // Setting programme to medium.
        model.prog = 16;
        prog = model.prog;
        cnt = model.cnt;
        state = model.state;
        assert.equal(prog, 16, "Programme should be 16 (short) now.");
        assert.equal(cnt, 15, "Laps should be 15 now.");
        assert.equal(state, 4, "State should be 4 (in) now.");

        // Setting programme to long.
        model.prog = 29;
        prog = model.prog;
        cnt = model.cnt;
        state = model.state;
        assert.equal(prog, 29, "Programme should be 29 (short) now.");
        assert.equal(cnt, 28, "Laps should be 28 now.");
        assert.equal(state, 4, "State should be 4 (in) now.");

        // Setting state to in.
        model.state = 4;
        state = model.state;
        let time = model.time;
        assert.equal(state, 4, "State should be 4 (in) now.");
        assert.equal(time, 4, "Time should be 4 now.");

        // Setting state to hold.
        model.state = 7;
        state = model.state;
        time = model.time;
        assert.equal(state, 7, "State should be 7 (hold) now.");
        assert.equal(time, 7, "Time should be 7 now.");

        // Setting state to out.
        model.state = 8;
        state = model.state;
        time = model.time;
        assert.equal(state, 8, "State should be 8 (out) now.");
        assert.equal(time, 8, "Time should be 8 now.");

    }
);


QUnit.test('[breathing page][model] Test setters with invalid values',
    assert => {
        const model = new BreathingModel();

        assert.throws(
            function() {
                model.time = -1;
            },
            "Time should not be negative."
        );

        assert.throws(
            function() {
                model.cnt = -2;
            },
            "Cnt should not be below -1."
        );
    }
);


QUnit.test('[breathing page][model] Test state swiching – short programme',
    assert => {
        const model = new BreathingModel();

        // Setting programme to short.
        model.prog = 10;

        model.Tick();
        let time = model.time;
        let cnt = model.cnt;
        assert.equal(time, 3, "Remaining time should be the starting time (4) - 1.");
        assert.equal(cnt, 9, "Remaining laps should be 10-1.");

        for(let i = 0; i < 4; i++){
            model.Tick();
        }

        time = model.time;
        let state = model.state;
        assert.equal(state, 7, "State should be 7 (hold).");
        assert.equal(time, 7, "Remaining time should be 7.");

        model.Tick();
        time = model.time;
        assert.equal(time, 6, "Remaining time should be the holding time (7) - 1.");

        for(let i = 0; i < 7; i++){
            model.Tick();
        }

        time = model.time;
        state = model.state;
        assert.equal(state, 8, "State should be 8 (out).");
        assert.equal(time, 8, "Remaining time should be 8.");

        model.Tick();
        time = model.time;
        assert.equal(time, 7, "Remaining time should be the holding time (8) - 1.");

        for(let i = 0; i < 8; i++){
            model.Tick();
        }

        time = model.time;
        state = model.state;
        assert.equal(state, 4, "State should be 4 (in).");
        assert.equal(time, 4, "Remaining time should be 4.");

        cnt = model.cnt;
        assert.equal(cnt, 8, "Remaining laps should be 8.");
        
    }
);

QUnit.test('[breathing page][model] Test state swiching – medium programme',
    assert => {
        const model = new BreathingModel();

        // Setting programme to medium.
        model.prog = 16;

        model.Tick();
        let time = model.time;
        let cnt = model.cnt;
        assert.equal(time, 3, "Remaining time should be the starting time (4) - 1.");
        assert.equal(cnt, 15, "Remaining laps should be 16-1.");

        for(let i = 0; i < 4; i++){
            model.Tick();
        }

        time = model.time;
        let state = model.state;
        assert.equal(state, 7, "State should be 7 (hold).");
        assert.equal(time, 7, "Remaining time should be 7.");

        model.Tick();
        time = model.time;
        assert.equal(time, 6, "Remaining time should be the holding time (7) - 1.");

        for(let i = 0; i < 7; i++){
            model.Tick();
        }

        time = model.time;
        state = model.state;
        assert.equal(state, 8, "State should be 8 (out).");
        assert.equal(time, 8, "Remaining time should be 8.");

        model.Tick();
        time = model.time;
        assert.equal(time, 7, "Remaining time should be the holding time (8) - 1.");

        for(let i = 0; i < 8; i++){
            model.Tick();
        }

        time = model.time;
        state = model.state;
        assert.equal(state, 4, "State should be 4 (in).");
        assert.equal(time, 4, "Remaining time should be 4.");

        cnt = model.cnt;
        assert.equal(cnt, 14, "Remaining laps should be 14.");
        
    }
);

QUnit.test('[breathing page][model] Test state swiching – long programme',
    assert => {
        const model = new BreathingModel();

        // Setting programme to long.
        model.prog = 29;

        model.Tick();
        let time = model.time;
        let cnt = model.cnt;
        assert.equal(time, 3, "Remaining time should be the starting time (4) - 1.");
        assert.equal(cnt, 28, "Remaining laps should be 29-1.");

        for(let i = 0; i < 4; i++){
            model.Tick();
        }

        time = model.time;
        let state = model.state;
        assert.equal(state, 7, "State should be 7 (hold).");
        assert.equal(time, 7, "Remaining time should be 7.");

        model.Tick();
        time = model.time;
        assert.equal(time, 6, "Remaining time should be the holding time (7) - 1.");

        for(let i = 0; i < 7; i++){
            model.Tick();
        }

        time = model.time;
        state = model.state;
        assert.equal(state, 8, "State should be 8 (out).");
        assert.equal(time, 8, "Remaining time should be 8.");

        model.Tick();
        time = model.time;
        assert.equal(time, 7, "Remaining time should be the holding time (8) - 1.");

        for(let i = 0; i < 8; i++){
            model.Tick();
        }

        time = model.time;
        state = model.state;
        assert.equal(state, 4, "State should be 4 (in).");
        assert.equal(time, 4, "Remaining time should be 4.");

        cnt = model.cnt;
        assert.equal(cnt, 27, "Remaining laps should be 27.");
        
    }
);

QUnit.test('[breathing page][model] Test length of short programme',
    assert => {
        const model = new BreathingModel();

        model.prog = 10;
        let cnt = model.cnt;

        let i=1;
        while(cnt>0)
        {
            model.Tick();
            if (cnt !== model.cnt)
            {
                cnt = model.cnt;
                i++;
            }
        }

        cnt = model.cnt;
        const prog = model.prog;
        const state = model.state;
        assert.equal(state, 0, "State should be 0 (ndef).");
        assert.equal(prog, 0, "Programme should be 0 (ndef).");
        assert.equal(i, 10, "Count of laps should be 10.");
        
    }
);

QUnit.test('[breathing page][model] Test length of medium programme',
    assert => {
        const model = new BreathingModel();

        model.prog = 16;
        let cnt = model.cnt;

        let i=1;
        while(cnt>0)
        {
            model.Tick();
            if (cnt !== model.cnt)
            {
                cnt = model.cnt;
                i++;
            }
        }

        cnt = model.cnt;
        const prog = model.prog;
        const state = model.state;
        assert.equal(state, 0, "State should be 0 (ndef).");
        assert.equal(prog, 0, "Programme should be 0 (ndef).");
        assert.equal(i, 16, "Count of laps should be 16.");
        
    }
);

QUnit.test('[breathing page][model] Test length of long programme',
    assert => {
        const model = new BreathingModel();

        model.prog = 29;
        let cnt = model.cnt;

        let i=1;
        while(cnt>0)
        {
            model.Tick();
            if (cnt !== model.cnt)
            {
                cnt = model.cnt;
                i++;
            }
        }

        cnt = model.cnt;
        const prog = model.prog;
        const state = model.state;
        assert.equal(state, 0, "State should be 0 (ndef).");
        assert.equal(prog, 0, "Programme should be 0 (ndef).");
        assert.equal(i, 29, "Count of laps should be 29.");
        
    }
);
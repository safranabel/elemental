

/* 
A test case should look like:

QUnit.test('[{pagename}][{class}] Test {details}',
    assert => {
        assert.true(true);
    }
);
*/


QUnit.test('[colouring page][model] Test default settings and getters',
    assert =>{
        const model = new ColouringModel();
        const model_fields = {
                actualPenType: model.getPenType(),
                colour: model.getColour(),
                penRadius: model.getPenRadius(),
                tableHeight: model.getHeigth(),
                tableWidth: model.getWidth(),
                lastCoordinates: model.getDrawableLine()[1],
                actualCoordinates: model.getDrawableLine()[0],
                rectRigthBottomCoords: model.getDrawableRect()[1],
                rectLeftTopCoords: model.getDrawableRect()[0]
        }

        assert.deepEqual(model_fields, 
            {
                actualPenType: PenType.standard,
                colour: '#FF0000',
                penRadius: 3,
                tableHeight: 300,
                tableWidth:400,
                lastCoordinates: [0,0],
                actualCoordinates: [0,0],
                rectRigthBottomCoords: [0,0],
                rectLeftTopCoords: [0,0]
        }, 'Model fields default values.');

    }
);

QUnit.test('[colouring page][model] Test set pen type',
    assert => {
        const model = new ColouringModel();
        model.setPenType(PenType.standard);
        assert.equal(model.getPenType(), PenType.standard, 'Pen type should be standard after set to standard.');

        model.setPenType(PenType.filler);
        assert.equal(model.getPenType(), PenType.filler, 'Pen type should be filler after set to filler.');

    }
);

QUnit.test('[colouring page][model] Test background colour changer',
    assert => {

        const model = new ColouringModel();

        addEventListener('rectRefreshed', (e:Event)=>{
            const rectCoords = model.getDrawableRect();
            const leftTop = rectCoords[0];
            const rightBottom = rectCoords[1];
            const coords = {
                leftTopX: leftTop[0],
                leftTopY: leftTop[1],
                rightBottomX: rightBottom[0],
                rightBottomY: rightBottom[1]
            }
            assert.deepEqual(coords, {
                leftTopX: 0,
                leftTopY: 0,
                rightBottomX: 400,
                rightBottomY: 300
            }, 'Check coords, when background in change.')
        });

        let rectCoords = model.getDrawableRect();
        let leftTop = rectCoords[0];
        let rightBottom = rectCoords[1];
        let coords = {
            leftTopX: leftTop[0],
            leftTopY: leftTop[1],
            rightBottomX: rightBottom[0],
            rightBottomY: rightBottom[1]
        }
        assert.deepEqual(coords, {
            leftTopX: 0,
            leftTopY: 0,
            rightBottomX: 0,
            rightBottomY: 0
        }, 'Check coords, before background changed.');
        
        model.changeBackgroundColour('#FFFFFF');
        
        rectCoords = model.getDrawableRect();
        leftTop = rectCoords[0];
        rightBottom = rectCoords[1];
        coords = {
            leftTopX: leftTop[0],
            leftTopY: leftTop[1],
            rightBottomX: rightBottom[0],
            rightBottomY: rightBottom[1]
        }
        assert.deepEqual(coords, {
            leftTopX: 0,
            leftTopY: 0,
            rightBottomX: 0,
            rightBottomY: 0
        }, 'Check coords, after background changed.');
    }

);

QUnit.test('[colouring page][model] Test constructor parameters',
    assert => {
        let model = new ColouringModel();
        assert.equal(model.getWidth(), 400, 'Drawing table width should be 400 by default.');
        assert.equal(model.getHeigth(), 300, 'Drawing table height should be 300 by default.');

        model = new ColouringModel(100);
        assert.equal(model.getWidth(), 400, 'Drawing table width should be 400 by default.');
        assert.equal(model.getHeigth(), 100, 'Drawing table height should be equal to the first parameter of the constructor.');

        model = new ColouringModel(30, 50);
        assert.equal(model.getWidth(), 50, 'Drawing table width should be equal to the second parameter of the constructor.');
        assert.equal(model.getHeigth(), 30, 'Drawing table height should be equal to the first parameter of the constructor.');
    }
);

QUnit.test('[colouring page][model] Test line drawing normal cases',
    assert => {
        const model = new ColouringModel();

        let countEventTrigger = 0;
        addEventListener('lineRefreshed',
            (e:Event) => {
                ++countEventTrigger;
            }
        );

        let lineCoords = model.getDrawableLine();
        let coords = {
            lineLastX: lineCoords[0][0],
            lineLastY: lineCoords[0][1],
            lineActX: lineCoords[1][0],
            lineActY: lineCoords[1][1]
        };
        

        assert.deepEqual(
            coords,
            {
                lineLastX: 0,
                lineLastY: 0,
                lineActX: 0,
                lineActY: 0
            },
            'Check default coords.'
        );


        model.startDrawing();
        model.penMovedTo([6,3]);
        lineCoords = model.getDrawableLine();
        coords = {
            lineLastX: lineCoords[0][0],
            lineLastY: lineCoords[0][1],
            lineActX: lineCoords[1][0],
            lineActY: lineCoords[1][1]
        };
        

        assert.deepEqual(
            coords,
            {
                lineLastX: 6,
                lineLastY: 3,
                lineActX: 6,
                lineActY: 3
            },
            'Check coords after first move.'
        );

        model.penMovedTo([9, 10]);
        lineCoords = model.getDrawableLine();
        lineCoords = model.getDrawableLine();
        coords = {
            lineLastX: lineCoords[0][0],
            lineLastY: lineCoords[0][1],
            lineActX: lineCoords[1][0],
            lineActY: lineCoords[1][1]
        };
        

        assert.deepEqual(
            coords,
            {
                lineLastX: 6,
                lineLastY: 3,
                lineActX: 9,
                lineActY: 10
            },
            'Check coords after second move.'
        );

        assert.equal(countEventTrigger, 2, 'Each time the pen moves, the event runs exactly once.');
    }
);

QUnit.test('[colouring page][model] Test line drawing faulty cases',
    assert => {
        const model = new ColouringModel();

        let countEventTrigger = 0;
        addEventListener('lineRefreshed',
            (e:Event) => {
                ++countEventTrigger;
            }
        );

        model.penMovedTo([6,3]);
        let lineCoords = model.getDrawableLine();
        let coords = {
            lineLastX: lineCoords[0][0],
            lineLastY: lineCoords[0][1],
            lineActX: lineCoords[1][0],
            lineActY: lineCoords[1][1]
        };
        

        assert.deepEqual(
            coords,
            {
                lineLastX: 0,
                lineLastY: 0,
                lineActX: 0,
                lineActY: 0
            },
            'Check coords when pen moved, but drawing has not started.'
        );

        model.startDrawing();
        model.penMovedTo([500,3]);
        lineCoords = model.getDrawableLine();
        coords = {
            lineLastX: lineCoords[0][0],
            lineLastY: lineCoords[0][1],
            lineActX: lineCoords[1][0],
            lineActY: lineCoords[1][1]
        };

        assert.deepEqual(
            coords,
            {
                lineLastX: 0,
                lineLastY: 0,
                lineActX: 0,
                lineActY: 0
            },
            'Check coords when pen moved, but to out of table width.'
        );

        model.penMovedTo([3,400]);
        lineCoords = model.getDrawableLine();
        coords = {
            lineLastX: lineCoords[0][0],
            lineLastY: lineCoords[0][1],
            lineActX: lineCoords[1][0],
            lineActY: lineCoords[1][1]
        };

        assert.deepEqual(
            coords,
            {
                lineLastX: 0,
                lineLastY: 0,
                lineActX: 0,
                lineActY: 0
            },
            'Check coords when pen moved, but to out of table height.'
        );

        model.penMovedTo([-5,3]);
        lineCoords = model.getDrawableLine();
        coords = {
            lineLastX: lineCoords[0][0],
            lineLastY: lineCoords[0][1],
            lineActX: lineCoords[1][0],
            lineActY: lineCoords[1][1]
        };

        assert.deepEqual(
            coords,
            {
                lineLastX: 0,
                lineLastY: 0,
                lineActX: 0,
                lineActY: 0
            },
            'Check coords when pen moved, but to out of table width.'
        );

        model.penMovedTo([3,-4]);
        lineCoords = model.getDrawableLine();
        coords = {
            lineLastX: lineCoords[0][0],
            lineLastY: lineCoords[0][1],
            lineActX: lineCoords[1][0],
            lineActY: lineCoords[1][1]
        };

        assert.deepEqual(
            coords,
            {
                lineLastX: 0,
                lineLastY: 0,
                lineActX: 0,
                lineActY: 0
            },
            'Check coords when pen moved, but to out of table height.'
        );

        assert.equal(countEventTrigger, 0, 'Each time the pen moves, the event runs exactly once.');
    }
);

QUnit.test('[colouring page][model] Test drawing stop',
    assert => {
        const model = new ColouringModel();

        let countEventTrigger = 0;
        addEventListener('lineRefreshed',
            (e:Event) => {
                ++countEventTrigger;
            }
        );


        model.penMovedTo([1, 1]);
        assert.equal(countEventTrigger, 0, 'Drawing should be stopped by default.');
        model.startDrawing();
        model.stopDrawing();
        model.penMovedTo([6, 3]);
        assert.equal(countEventTrigger, 0, 'Event should not be triggerd when drawing is in stopped state.');
        model.penMovedTo([9, 10]);
        assert.equal(countEventTrigger, 0, 'Event should not be triggerd when drawing is in stopped state.');

        model.startDrawing();
        model.penMovedTo([6, 3]);
        model.penMovedTo([9, 10]);
        assert.equal(countEventTrigger, 2, 'Each time the pen moves, the event runs exactly once.');
        model.stopDrawing();
        model.penMovedTo([9, 10]);
        model.penMovedTo([6, 3]);
        assert.equal(countEventTrigger, 2, 'Event should not be triggerd when drawing is in stopped state.');

    }
);

QUnit.test('[colouring page][model] Test change pen radius normal cases',
    assert => {
        const model = new ColouringModel();

        assert.equal(model.getPenRadius(), 3, 'Pen radius should be 3 by default.');

        model.increasePenRadius(5);
        assert.equal(model.getPenRadius(), 8, 'Pen radius should increase by 5.');

        model.increasePenRadius(-2);
        assert.equal(model.getPenRadius(), 6, 'Pen radius should drop by 2.');

        model.increasePenRadius(2);
        assert.equal(model.getPenRadius(), 8, 'Pen radius should increase by 2.');


    }
);

QUnit.test('[colouring page][model] Test change pen radius faulty cases',
    assert => {
        const model = new ColouringModel();

        model.increasePenRadius(-300);

        model.increasePenRadius(-2);
        assert.equal(model.getPenRadius(), 2, 'Pen radius should not go under 2.');

        model.increasePenRadius(30);
        assert.equal(model.getPenRadius(), 30, 'Pen radius should not go over 30.');
    }
);

QUnit.test('[colouring page][model] Test change pen colour normal cases',
    assert => {
        const model = new ColouringModel();

        assert.equal(model.getColour(), '#FF0000', 'Pen should be red by default.');

        model.changePenColour('#1Fa');
        assert.equal(model.getColour(), '#1Fa', 'Pen should change its colour when valid value comes.');

        model.changePenColour('#FaFaBB');
        assert.equal(model.getColour(), '#FaFaBB', 'Pen should change its colour when valid value comes.');
    }
);

QUnit.test('[colouring page][model] Test change pen colour faulty cases',
    assert => {
        const model = new ColouringModel();

        model.changePenColour('#Fa');
        assert.equal(model.getColour(), '#FF0000', 'Pen should not change its colour when invalid value comes.');

        model.changePenColour('#');
        assert.equal(model.getColour(), '#FF0000', 'Pen should not change its colour when invalid value comes.');

        model.changePenColour('FFF#');
        assert.equal(model.getColour(), '#FF0000', 'Pen should not change its colour when invalid value comes.');

        model.changePenColour('FFF');
        assert.equal(model.getColour(), '#FF0000', 'Pen should not change its colour when invalid value comes.');

        model.changePenColour('');
        assert.equal(model.getColour(), '#FF0000', 'Pen should not change its colour when invalid value comes.');

    }
);
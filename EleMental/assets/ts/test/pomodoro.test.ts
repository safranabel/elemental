/* 
A test case should look like:

QUnit.test('[{pagename}][{class}] Test {details}',
    assert => {
        assert.true(true);
    }
);
*/

QUnit.test('[pomodoro page][model] Test default settings',
    assert => {
        const model = new PomodoroModel();

        const studyTime = model.getStudyTime();
        const studyMinutes = model.getStudyMinutes();
        const remainingTime = model.getRemainingTime();
        const breakTime = model.getBreakTime();
        const breakMinutes = model.getBreakMinutes();
        const startingIntervalType = model.getIntervalType();
        const startingIntervalStatus = model.getIntervalStatus();

        assert.equal(studyTime, 1500, "Default study time should be 1500 seconds.");
        assert.equal(studyMinutes, 25, "Default study minutes should be 25 minutes.");
        assert.equal(remainingTime, 1500, "Timer has not been started; remaining time should be equal to study time.");
        assert.equal(breakTime, 300, "Default break time should be 300 seconds.");
        assert.equal(breakMinutes, 5, "Default break minutes should be 5 minutes.");
        assert.equal(startingIntervalType, IntervalType.study, "Starting interval type should be a study.");
        assert.equal(startingIntervalStatus, IntervalStatus.off, "Starting interval status should be off.");
    }
);

QUnit.test('[pomodoro page][model] Test setters',
    assert => {
        const model = new PomodoroModel();

        // Setting study time to 30 minutes instead of 25.
        model.setStudyTime(1800);
        const studyTime = model.getStudyTime();
        assert.equal(studyTime, 1800, "Study time should be 1800 seconds now.");

        // Setting break time to 10 minutes instead of 5.
        model.setBreakTime(600);
        const breakTime = model.getBreakTime();
        assert.equal(breakTime, 600, "Break time should be 600 seconds now.");

        model.setRemainingTime(600);
        const remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 600, "Remaining time should be 600 seconds now.");

        model.setIntervalStatus(IntervalStatus.off);
        let intervalStatus = model.getIntervalStatus();
        assert.equal(intervalStatus, IntervalStatus.off, "Interval status should be 'off'.");

        model.setIntervalStatus(IntervalStatus.on);
        intervalStatus = model.getIntervalStatus();
        assert.equal(intervalStatus, IntervalStatus.on, "Interval status should be 'on'.");

        model.setStudyMinutes(32);
        const studyMinutes = model.getStudyMinutes();
        assert.equal(studyMinutes, 32, "Study minutes should be 32.");

        model.setBreakMinutes(12);
        const breakMinutes = model.getBreakMinutes();
        assert.equal(breakMinutes, 12, "Break minutes should be 12.");

        
    }
);

QUnit.test('[pomodoro page][model] Test setting study interval',
    assert => {
        const model = new PomodoroModel();

        model.increaseStudyMinutes();
        let studyMinutes = model.getStudyMinutes();
        assert.equal(studyMinutes, 32, "Study minutes should be 32 minutes now.");

        for(let i = 0; i < 10; i++){
            model.increaseStudyMinutes();
        }
        studyMinutes = model.getStudyMinutes();
        assert.equal(studyMinutes, 53, "Study minutes should be 53 minutes now, that is the maximum minutes that can be chosen.");

        model.decreaseStudyMinutes();
        studyMinutes = model.getStudyMinutes();
        assert.equal(studyMinutes, 46, "Study minutes should be 46 minutes now.");

        for(let i = 0; i < 10; i++){
            model.decreaseStudyMinutes();
        }
        studyMinutes = model.getStudyMinutes();
        assert.equal(studyMinutes, 4, "Study minutes should be 4 minutes now, that is the minimum minutes that can be chosen.");
    }
);

QUnit.test('[pomodoro page][model] Test setting break interval',
    assert => {
        const model = new PomodoroModel();

        model.increaseBreakMinutes();
        let breakMinutes = model.getBreakMinutes();
        assert.equal(breakMinutes, 12, "Break minutes should be 12 minutes now.");

        for(let i = 0; i < 15; i++){
            model.increaseBreakMinutes();
        }
        breakMinutes = model.getBreakMinutes();
        assert.equal(breakMinutes, 40, "Break minutes should be 40 minutes now, that is the maximum minutes that can be chosen.");

        model.decreaseBreakMinutes();
        breakMinutes = model.getBreakMinutes();
        assert.equal(breakMinutes, 33, "Break minutes should be 33 minutes now.");

        for(let i = 0; i < 15; i++){
            model.decreaseBreakMinutes();
        }
        breakMinutes = model.getBreakMinutes();
        assert.equal(breakMinutes, 5, "Break minutes should be 5 minutes now, that is the minimum minutes that can be chosen.");
    }
);

QUnit.test('[pomodoro page][model] Test passing of time',
    assert => {
        const model = new PomodoroModel();

        model.passTime();
        let remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 1500, "Remaining time should be the starting time.");

        model.setIntervalStatus(IntervalStatus.on);
        model.passTime();
        remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 1499, "Remaining time should be 1499 seconds now.");

        for(let i = 0; i < 1510; i++){
            model.passTime();
        }
        remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 300, "Remaining time should be equal to the break time.");

        model.setIntervalStatus(IntervalStatus.on);
        model.passTime();
        remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 299, "Remaining time should be 299 seconds now.");

        for(let i = 0; i < 310; i++){
            model.passTime();
        }
        remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 1500, "Remaining time should be equal to the study time.");
    }
);

QUnit.test('[pomodoro page][model] Test starting timer',
    assert => {
        const model = new PomodoroModel();

        model.startTimer();
        const intervalStatus = model.getIntervalStatus();
        assert.equal(intervalStatus, IntervalStatus.on, "Interval status should be 'on'.");
   }
);

QUnit.test('[pomodoro page][model] Test pausing timer',
    assert => {
        const model = new PomodoroModel();

        model.pauseTimer();
        const intervalStatus = model.getIntervalStatus();
        assert.equal(intervalStatus, IntervalStatus.off, "Interval status should be 'off'.");
   }
);

QUnit.test('[pomodoro page][model] Test skipping interval',
    assert => {
        const model = new PomodoroModel();

        model.skipInterval();
        let intervalStatus = model.getIntervalStatus();
        assert.equal(intervalStatus, IntervalStatus.off, "Interval status should be 'off'.");

        let intervalType = model.getIntervalType();
        assert.equal(intervalType, IntervalType.break, "Interval type should be 'break'.");

        let remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 300, "Remaining time should be equal to the break time.");

        model.startTimer();
        model.passTime();
        model.skipInterval();

        intervalStatus = model.getIntervalStatus();
        assert.equal(intervalStatus, IntervalStatus.off, "Interval status should be 'off'.");

        intervalType = model.getIntervalType();
        assert.equal(intervalType, IntervalType.study, "Interval type should be 'study'.");

        remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 1500, "Remaining time should be equal to the study time.");

        model.startTimer();
        model.passTime();
        model.passTime();
        model.passTime();
        model.pauseTimer();
        model.skipInterval();

        intervalStatus = model.getIntervalStatus();
        assert.equal(intervalStatus, IntervalStatus.off, "Interval status should be 'off'.");

        intervalType = model.getIntervalType();
        assert.equal(intervalType, IntervalType.break, "Interval type should be 'study'.");

        remainingTime = model.getRemainingTime();
        assert.equal(remainingTime, 300, "Remaining time should be equal to the break time.");
   }
);
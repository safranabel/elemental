# EleMental
>*Célunk egy olyan webalkalmazás fejlesztése, mely segít a Felhasználónak mentális egészsége megőrzésében, helyreállításában.*

## Telepítés, build, tesztelés, extra eszközök
### Telepítési folyamat
Az alkalmazás működésének előfeltétele, hogy a kedves Felhasználó eszközén telepítve legyen egy ```node.js``` keretrendszer. Amennyiben ez nem áll rendekezésünkre, az alábbi útmutató segítségével beszerezhetjük: [**node.js telepítési útmutató**](https://nodejs.org/en/download/).  
Amennyiben minden vágyunk az, hogy csomagkezelőn keresztül töltsük le ```node.js```-ünket, az alábbi linken találunk leírást: [**node.js telepítése csomagkezelővel**](https://nodejs.org/en/download/package-manager/).

Amint rendelkezésre áll a fent említett keretrendszerünk, már csak néhány parancs futtatása választ el minket a használattól. Ezek a parancsok a következők:  
 - ```npm install -g typescript```
 - ```npm install qunit``` ← *(teszteléshez)*
 - ```npm install -g node-qunit-phantomjs``` ← *(teszteléshez)*

### Build
A buildelés végtelenül egyszerű, mindössze egyetlen parancsot kell bepötyögnünk, ez pedig nem más, mint az ```npm run build```.

### Tesztelés
A tesztelés sem bonyolult. A buildelést követően a következő karaktertömbbel kell kidekorálni parancssorunkat: ```npm run test```.

### Extra eszközök
Bár a build fázis automatikusan használja extra eszközeinket, az alábbi parancsok elrendelésével ki tudjuk ragadni ezen eszközök futtatását az egyéb parancsok közül:  
 - ```npm run lint``` ← *(ES Lint)*
 - ```npm run coverage``` ← *(TS Coverage) – ez még nem üzemel tökéletesen*

## A weboldal működéséről

### Főoldal
A főoldalra beköszönvén bolygókkal találjuk szembe magunkat. Ezen égitestekre kattintva leszállhatunk a kiválasztott planétára, és kipróbálhatjuk az ott fellelhető funkciókat. A bolygók tehát menüként működnek.

### A lila bolygó – Pomodoro
E planéta arra hivatott szolgálni, hogy elősegítse a tanulási folyamatokat. A közismert [**pomodoro**](https://en.wikipedia.org/wiki/Pomodoro_Technique) technikához nyújt időzítőt, melynek értékeit kedvére állítgathatja az okulni vágyó Látogató.

### A kék bolygó – Légzésgyakorlatok
Ha nyugalomra vágyik a Szemlélődő ezekben a sűrű hétköznapokban, könnyed kikapcsolódási lehetőséget nyújt erre ez a bolygó. Amint kiválsztottuk, mennyi időt szeretnénk meditációnkra fordítani, csupán követni kell a planéta légkörének sugallatait.

### A színpompás bolygó – Színező
Ha aktívabban vágyunk szórakozni, ezen aloldalra utazva újraélhetjük gyermekkorunkat az itt fellelhető színező nyomkodásával. Választhatunk hátteret és tollszínt a vászon alatti festékpamacs-szigetekre való klikkeléssel, valamint tollunk vastagságának állítására is lehetőséget kapunk a vászont tartalmazó sziget bal oldalán leldző ```+```, illetve ```-``` gombok használatával. 
